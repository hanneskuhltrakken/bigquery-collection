WITH
  data AS (
  SELECT
    hits.page.pagePath AS landing_page,
    LEAD(hits.page.pagePath) OVER session second_page,
    LEAD(hits.page.pagePath,2) OVER session third_page,
    LEAD(hits.page.pagePath,3) OVER session fourth_page,
    LEAD(hits.page.pagePath,4) OVER session fifth_page
  FROM
    `bigquery-public-data.google_analytics_sample.ga_sessions_*`,
    UNNEST(hits) AS hits
  WHERE
    hits.type = "PAGE"
  WINDOW
    session as (partition by fullvisitorid, visitid order by hits.time)
)
SELECT
  *,
  COUNT(*) AS session_count
FROM
  data
WHERE
  landing_page = '/home'
GROUP BY
  1,
  2,
  3,
  4,
  5
ORDER BY
  session_count DESC