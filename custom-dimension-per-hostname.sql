CREATE TEMP FUNCTION
  customDimensionByIndex(indx INT64,
    arr ARRAY<STRUCT<index INT64,
    value STRING>>) AS ( (
    SELECT
      x.value
    FROM
      UNNEST(arr) x
    WHERE
      indx=x.index) );

SELECT
    DISTINCT hits.page.hostname as hostname,
    COUNTIF(hits.type = 'PAGE') AS total_pageviews,
    customDimensionByIndex(3, hits.customDimensions) AS CDIM1
        
  FROM
    `redbull-mediahouse-bq.130514671.ga_sessions_20190909` AS GA,
    UNNEST(GA.hits) AS hits
    WHERE hits.type = 'PAGE'

group by hostname, CDIM1 ORDER BY hostname desc, total_pageviews desc