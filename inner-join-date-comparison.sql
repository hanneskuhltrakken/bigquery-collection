-- declare the tables that are to be joined
-- the 'with' function can be used to declare
-- temporal tables that can be used in later joins
WITH
  data AS (
  SELECT
    channelGrouping,
    COUNT(CONCAT(fullvisitorid, CAST(visitid AS string))) AS sessions,
  FROM
    `bigquery-public-data.google_analytics_sample.ga_sessions_2017*`,
    UNNEST(hits) AS hits
  WHERE
    _TABLE_SUFFIX BETWEEN '0101'
    AND '0107'
  GROUP BY
    1),
  data2 AS (
  SELECT
    channelGrouping,
    COUNT(CONCAT(fullvisitorid, CAST(visitid AS string))) AS sessions,
  FROM
    `bigquery-public-data.google_analytics_sample.ga_sessions_2017*`,
    UNNEST(hits) AS hits
  WHERE
    _TABLE_SUFFIX BETWEEN '0108'
    AND '0114'
  GROUP BY
    1 )

SELECT
  data.channelGrouping,
  data.sessions as sessions_1,
  data2.sessions as sessions_2,
  safe_divide(data.sessions,
    data2.sessions) AS sessions_growth
FROM
  data
INNER JOIN
  data2
ON
  data.channelGrouping = data2.channelGrouping