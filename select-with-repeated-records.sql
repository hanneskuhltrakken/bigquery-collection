SELECT
  date,
  visitId,
  fullVisitorId,
  visitStartTime,
  visitNumber,
  (SELECT STRUCT(totals.timeOnSite)) as totals,
  (SELECT STRUCT(trafficSource.referralPath, trafficSource.source, trafficSource.medium)) as trafficSource,
  (SELECT STRUCT(device.deviceCategory)) as device
FROM
  `wuerth-2019-09.201586137.ga_sessions_20190923`
ORDER BY
  totals.timeOnSite DESC
  #WHERE
  # _TABLE_SUFFIX = FORMAT_DATE('%Y%m&d', DATE_SUB(@run_date, INTERVAL 1 DAY))