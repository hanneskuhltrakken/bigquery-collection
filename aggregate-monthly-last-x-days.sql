SELECT 
DATE_TRUNC(PARSE_DATE('%Y%m%d',date), MONTH) as MonthStart,
device.browser,
device.browserVersion,
SUM(totals.visits) as visits,
SUM(totals.newVisits) as newVisits,
SUM(totals.newVisits) / SUM(totals.visits) as ratio
from `redbull-mediahouse-bq.130514671.ga_sessions_*` 
where
device.browser in ('Safari', 'Chrome', 'Firefox', 'Internet Explorer', 'Safari (in-app)')
AND
_TABLE_SUFFIX between FORMAT_DATE('%Y%m&d', DATE_SUB(CURRENT_DATE(), INTERVAL 600 DAY))
AND
FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
GROUP BY MonthStart, device.browser, device.browserVersion
ORDER BY MonthStart ASC
