SELECT 
clientId,
device.browser,
MIN(visitStartTime) as first_visit,
MAX(visitStartTime) as last_visit,
# get the first of each clientIds minimum visistStartTime 
DATE_TRUNC( DATE(TIMESTAMP_SECONDS(MIN(visitStartTime))), MONTH) as MonthStart,

# get the difference between a clientId's max and min start time 
# then divide it by the amount of seconds in a day (60*60*24=86400)
(MAX(visitStartTime)  - MIN(visitStartTime) )/86400 as cookie_lifetime_in_days,
SUM(totals.visits) as visits
from `redbull-mediahouse-bq.169050698.ga_sessions_*` 
where
clientId IS NOT NULL
AND
_TABLE_SUFFIX between FORMAT_DATE('%Y%m&d', DATE_SUB(CURRENT_DATE(), INTERVAL 500 DAY))
AND
FORMAT_DATE('%Y%m%d',DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
GROUP BY clientId, device.browser
