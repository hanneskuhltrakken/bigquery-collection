CREATE TEMP FUNCTION
  customDimensionByIndex(indx INT64,
    arr ARRAY<STRUCT<index INT64,
    value STRING>>) AS ( (
    SELECT
      x.value
    FROM
      UNNEST(arr) x
    WHERE
      indx=x.index) );

SELECT
    DISTINCT hits.page.pagePath, hits.sourcePropertyInfo.sourcePropertyDisplayName, hits.page.hostname,
    COUNTIF(hits.type = 'PAGE') AS total_pageviews,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(1, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(1, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_1_complete_ratio,

    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(2, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(2, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_2_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(3, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(3, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_3_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(4, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(4, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_4_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(5, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(5, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_5_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(6, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(6, hits.customDimensions) LIKE 'unspecified'),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_6_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(7, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(7, hits.customDimensions) LIKE 'unspecified' 
                OR customDimensionByIndex(7, hits.customDimensions) LIKE ''),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_7_complete_ratio,
    ROUND(
        100 - (SAFE_DIVIDE(
            COUNTIF(
                customDimensionByIndex(9, hits.customDimensions) IS NULL 
                OR customDimensionByIndex(9, hits.customDimensions) LIKE 'unspecified' 
                OR customDimensionByIndex(9, hits.customDimensions) LIKE ''),
            COUNTIF(hits.type = 'PAGE'
        )) * 100)
        , 2
    ) as cdim_9_complete_ratio
        
  FROM
    `redbull-mediahouse-bq.130514671.ga_sessions_*` AS GA,
    UNNEST(GA.hits) AS hits
    WHERE hits.dataSource = 'web'
      AND hits.type = 'PAGE'
      AND hits.page.hostname LIKE "%redbull%"
      AND _TABLE_SUFFIX BETWEEN 
        FORMAT_DATE('%Y%m&d', DATE_SUB(CURRENT_DATE(), INTERVAL 14 DAY)) 
        AND FORMAT_DATE('%Y%m%d',CURRENT_DATE())

group by 1, 2, 3 ORDER BY hostname desc, total_pageviews desc